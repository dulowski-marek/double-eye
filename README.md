# Double - Eye website #

## Disclaimer ##
This is a project undergoing construction. It aims to present the workflow I implement doing personal projects.

## Git workflow ##
History is linear due to rebase-merge workflow. I am also able to work with merge --no-ff to maintain commit history accordingly to expectations.
